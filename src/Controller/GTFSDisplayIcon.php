<?php

namespace Drupal\gtfs_display\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\BareHtmlPageRenderer;
use Drupal\Core\Render\HtmlResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * An example controller.
 */
class GTFSDisplayIcon extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function content($icon) {

    $file = drupal_get_path('module', 'gtfs_display') . '/img/' . $icon . '-11.svg';

    if(!file_exists($file)) {
      throw new NotFoundHttpException('Icon not found');
    }

    $query = \Drupal::request()->query->all();

    $allowed_properties = [
      'fill',
      'stroke',
      'stroke-width'
    ];

    $query = array_filter($query, function ($value, $key) use ($allowed_properties) {
      return in_array($key, $allowed_properties) && (is_string($value) || is_numeric($value));
    }, ARRAY_FILTER_USE_BOTH);

    $query['fill'] = $query['fill'] ?? 'black';

    $svg = file_get_contents($file);

    $style = "<style>path { " . implode("\n", array_map(function ($property) use ($query) {
        return "{$property}: {$query[$property]};";
      }, array_keys($query))) . " }</style>";

    $svg = str_replace('</svg>', "{$style}</svg>", $svg);

    $response = HtmlResponse::create($svg);

    $response->headers->set('Content-Type', 'image/svg+xml');

    foreach ($allowed_properties as $allowed_property) {
      $response->getCacheableMetadata()->addCacheContexts(['url.query_args:' . $allowed_property]);
    }



    return $response;
  }

}
