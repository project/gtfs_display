<?php

namespace Drupal\gtfs_display\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\BareHtmlPageRenderer;

/**
 * An example controller.
 */
class GTFSDisplayController extends ControllerBase {

  public function title() {
    return 'hello';
  }

  /**
   * Returns a render-able array for a test page.
   */
  public function content() {
    $title = 'testing';
    $parameters = \Drupal::routeMatch()->getParameters();
    $route = \Drupal::routeMatch()->getRouteObject();
    $path = $route->getPath();
    $name = \Drupal::routeMatch()->getRouteName();

    $type_id = str_replace('gtfs_display.', '', $name);
    $display_type = \Drupal::routeMatch()->getParameter('display_type');

    return \Drupal::service('gtfs_display.renderer')->render($type_id, $display_type);
  }

}
