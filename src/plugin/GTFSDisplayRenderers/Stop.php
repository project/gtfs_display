<?php

namespace Drupal\gtfs_display\Plugin\GTFSDisplayRenderers;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\gtfs\Entity\GTFSEntityBase;

class Stop extends Base {
  public static function build(GTFSEntityBase $stop, &$build = []) {
    $build = parent::build($stop, $build);
    $build['#markup'] = '';
    $build['container']['#type'] = 'container';
    $build['container']['#attributes'] = ['class' => 'container'];
    $build['container']['title'] = [
      '#markup' => '<h1>' . $stop->label() . '</h1>'
    ];
    $build['container']['routes'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#title' => 'Routes',
      '#items' => array_values(array_map(function (\Drupal\gtfs\Entity\Route $route) {
        return Link::fromTextAndUrl($route->label(), Url::fromUri($route->links()['display']));
      }, $stop->routes()))
    ];
    return $build;
  }

}