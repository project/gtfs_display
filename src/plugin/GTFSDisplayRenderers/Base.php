<?php

namespace Drupal\gtfs_display\Plugin\GTFSDisplayRenderers;

use Drupal\gtfs\Entity\GTFSEntityBase;
use Drupal\gtfs_display\Form\GTFSDisplayConfigForm;

class Base {
  public static function build(GTFSEntityBase $instance, &$build = []) {
    $build['#markup'] = 'No custom renderer registered for ' . static::class;
    $build['#attached']['library'][] = 'gtfs_display/base';
    $build['#attached']['drupalSettings']['gtfs_display']['mapboxToken'] = \Drupal::config(GTFSDisplayConfigForm::SETTINGS)->get('mapbox_access_token');
    return $build;
  }
}