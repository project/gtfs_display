<?php

namespace Drupal\gtfs_display\Plugin\GTFSDisplayRenderers;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\gtfs\Entity\GTFSEntityBase;

class Agency extends Base {
  public static function build(GTFSEntityBase $agency, &$build = []) {
    $build = parent::build($agency, $build);
    $build['#markup'] = '';
    $build['container'] = [
      '#type' => 'container',
      '#attributes' => ['class' => 'container'],
      'title' => [
        '#markup' => '<h1>' . $agency->label() . '</h1>'
      ],
      'table' => [
        '#type' => 'table',
        '#header' => ['Route ID', 'Route Name'],
        '#rows' => array_values(array_map(function (\Drupal\gtfs\Entity\Route $route) {
          return [
            $route->get('route_id')->value,
            Link::fromTextAndUrl($route->get('route_long_name')->value, Url::fromUri($route->links()['display']))
          ];
        }, $agency->routes()))
      ]
    ];
    return $build;
  }

}