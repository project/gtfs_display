<?php

namespace Drupal\gtfs_display\Plugin\GTFSDisplayRenderers;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\gtfs\Entity\GTFSEntityBase;

class Route extends Base {
  public static function build(GTFSEntityBase $route, &$build = []) {
    $build = parent::build($route, $build);
    $build['#markup'] = '';
    $build['container'] = [
      '#type' => 'container',
      '#attributes' => ['class' => 'container'],
      'title' => [
        '#markup' => '<h1>' . $route->label() . '</h1>'
      ],
      'table' => [
        '#type' => 'table',
        '#header' => [
          'stop_id' => 'Stop ID',
          'stop_name' => 'Stop Name',
          'route' => 'Routes at Stop'
        ],
        '#rows' => array_values(array_map(function (\Drupal\gtfs\Entity\Stop $stop) {
          return [
            'stop_id' => [ 'data' => $stop->get('stop_id')->value ],
            'stop_name' => [ 'data' => Link::fromTextAndUrl($stop->label(), Url::fromUri($stop->links()['display'])) ],
            'route' => [
              'data' => [
                '#markup' => implode(', ', array_map(function (\Drupal\gtfs\Entity\Route $route) {
                  return Link::fromTextAndUrl($route->label(), Url::fromUri($route->links()['display']))->toString();
                }, $stop->routes())),
              ]
            ]

          ];
        }, $route->stops()))
      ]
    ];
    return $build;
  }

}