<?php

namespace Drupal\gtfs_display\Plugin\GTFSDisplayRenderers;

use Drupal\gtfs\Entity\GTFSEntityBase;

class Trip extends Base {
  public static function build(GTFSEntityBase $trip, &$build = []) {
    $build = parent::build($trip, $build);
    $build['#markup'] = 'A nice trip representation';
    return $build;
  }

}