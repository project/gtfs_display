<?php

namespace Drupal\gtfs_display\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 */
class GTFSDisplayConfigForm extends ConfigFormBase {
  const SETTINGS = 'gtfs_display.settings';

  /**
   *
   */
  public function getFormId() {
    return 'gtfs_display_settings';
  }

  /**
   *
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);


    return parent::buildForm($form, $form_state);
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
