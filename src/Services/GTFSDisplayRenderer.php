<?php

namespace Drupal\gtfs_display\Services;

use Drupal\Core\Render\BareHtmlPageRenderer;
use Drupal\gtfs\Entity\GTFSEntityBase;
use Drupal\gtfs\Entity\HasIdTrait;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GTFSDisplayRenderer
 *
 * @package Drupal\gtfs_display\Services
 */
class GTFSDisplayRenderer {
  public function render(string $type_id, $display_type): Response {
    /** @var \Drupal\Core\Entity\EntityTypeManager $manager */
    $manager = \Drupal::service('entity_type.manager');
    $storage = $manager->getStorage('gtfs_' . $type_id);
    $id = \Drupal::routeMatch()->getParameter("{$type_id}_id");
    $parent_parameters = array_diff(
      \Drupal::routeMatch()->getParameters()->keys(),
      ['display_type', "{$type_id}_id"]
    );

    // We have to get the trail of parents, because nested entities require the parent to load by id
    $lastParent = false;

    foreach ($parent_parameters as $parent_parameter) {
      $type = str_replace('_id', '', $parent_parameter);
      $definition = $manager->getDefinition('gtfs_' . $type);
      $class = $definition->getClass();
      $lastParent = call_user_func_array(
        [$class, 'getById'],
        array_filter([$lastParent, \Drupal::routeMatch()->getParameter($parent_parameter)])
      );
    }

    $definition = $manager->getDefinition('gtfs_' . $type_id);
    $class = $definition->getClass();

    $hasIdTrait = in_array(
      HasIdTrait::class,
      array_keys((new \ReflectionClass($class))->getTraits())
    );


    if ($hasIdTrait) {
      try {
        $instance = call_user_func_array(
          [$class, 'getById'],
          array_filter([$lastParent, $id])
        );
      } catch (\Throwable $e) {}
    }

    if (!isset($instance)) {
      throw new \Exception('Ooops dude');
      $instance = $storage->load($id);
    }

    $title = $instance->label();

    \Drupal::moduleHandler()->alter("gtfs_display_{$type_id}_title", $title, $instance, $display_type);

    $build = [
      '#markup' => 'No handler registered for ' . $type_id,
      '#attached' => [
        'html_head' => [
          [
            [
              '#tag' => 'base',
              '#attributes' => [
                'target' => '_parent'
              ],
            ],
            'base'
          ]
        ]
      ]
    ];

    \Drupal::moduleHandler()->alter([
      "gtfs_display_render",
      "gtfs_display_{$type_id}_render",
      "gtfs_display_{$display_type}_render",
      "gtfs_display_{$type_id}_{$display_type}_render"
    ], $build, $instance, $display_type);

    $attachments = \Drupal::service('html_response.attachments_processor');
    $renderer = \Drupal::service('renderer');

    $bareHtmlPageRenderer = new BareHtmlPageRenderer($renderer, $attachments);

    $response = $bareHtmlPageRenderer->renderBarePage($build, $title, 'markup');

    \Drupal::moduleHandler()->alter([
      "gtfs_display_render",
      "gtfs_display_{$type_id}_response",
      "gtfs_display_{$display_type}_response",
      "gtfs_display_{$type_id}_{$display_type}_response"
    ], $response, $instance, $display_type);

    return $response;
  }
}