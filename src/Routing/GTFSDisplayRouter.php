<?php

namespace Drupal\gtfs_display\Routing;

use Symfony\Component\Routing\Route;

/**
 * Defines dynamic routes.
 */
class GTFSDisplayRouter {

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $routes = [];
    $types = \Drupal::service('entity_type.manager')->getDefinitions();
    /** @var \Drupal\Core\Entity\EntityTypeInterface $type */
    foreach ($types as $type) {
      $is_gtfs_type = (strpos($type->id(), 'gtfs_') !== FALSE);
      if ($is_gtfs_type) {
        $class = $type->getClass();
        if (!property_exists($class, 'resourceURL')) continue;
        $root = str_replace('/gtfs/api/v1/', '', $class::$resourceURL);
        $type_id = str_replace('gtfs_', '', $type->id());
        $label = $type->getLabel();
        $routes['gtfs_display.' . $type_id] = new Route(
          "/gtfs/display/{$root}/{{$type_id}_id}/{display_type}",
          [
            '_controller' => '\Drupal\gtfs_display\Controller\GTFSDisplayController::content',
            '_title_callback' => '\Drupal\gtfs_display\Controller\GTFSDisplayController::title',
            'display_type' => 'default'
          ],
          [
            '_permission' => "view published gtfs {$type_id} entities"
          ]
        );
      }
    }
    return $routes;
  }

}
