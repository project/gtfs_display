const getQuery = () => [...new URLSearchParams(window.location.search).entries()].reduce((q, [k, v]) => Object.assign(q, {[k]: v}), {});
console.info(`Data for this page is available at %c${window.location.href.replace('display', 'api/v1')}`, `display: block;color: white;text-decoration: underline`);


function displayLink(entity) {
  return entity.self.replace('api/v1', 'display');
}


function renderEntityGeoJSON(entity, type) {
  const geoJSONOptionsByType = {
    route: {
      style: function (feature) {
        let color = entity.route_color;
        if (color) {
          if (!color.includes('#')) color = `#${color}`;
        } else {
          color = randomColor();
        }
        return { color };
      }
    },
    default: {}
  };

  const json = L.geoJSON(
      entity.geojson,
      geoJSONOptionsByType[type] || geoJSONOptionsByType.default
  );

  return json;
}